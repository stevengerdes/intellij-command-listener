package code;

import com.google.common.base.Suppliers;
import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.TextBrowseFolderListener;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.ui.components.JBLabel;
import org.fest.util.Strings;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class CommandListenerSettings implements Configurable {
	private static final String FILE_PATH_KEY = "intellijCommandListener.filePath";
	private static final Map<String, Supplier<File>> s_fileSupplierCache = new HashMap<>();

	private final PropertiesComponent _propertiesComponent;
	private final TextFieldWithBrowseButton _textFieldWithBrowseButton = new TextFieldWithBrowseButton();
	private final Project _project;

	public CommandListenerSettings(Project project) {
		_project = project;
		_propertiesComponent = PropertiesComponent.getInstance(project);
		if (!s_fileSupplierCache.containsKey(_project.getLocationHash())) {
			s_fileSupplierCache.put(_project.getLocationHash(),
									Suppliers.memoize(this::getFile));
		}
	}

	@Override
	public String getDisplayName() {
		return "Command Listener";
	}

	@Override
	public JComponent createComponent() {
		JPanel settingsPanel = new JPanel();
		settingsPanel.setLayout(new BoxLayout(settingsPanel,
											  BoxLayout.X_AXIS));
		settingsPanel.add(new JBLabel("Communication File:"));

		_textFieldWithBrowseButton.addBrowseFolderListener(new TextBrowseFolderListener(new FileChooserDescriptor(true,
																												  false,
																												  false,
																												  false,
																												  false,
																												  false)));
		_textFieldWithBrowseButton.setText(_propertiesComponent.getValue(FILE_PATH_KEY));
		_textFieldWithBrowseButton.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
		settingsPanel.add(_textFieldWithBrowseButton);

		return settingsPanel;
	}

	@Override
	public boolean isModified() {
		return !_textFieldWithBrowseButton.getText().equals(_propertiesComponent.getValue(FILE_PATH_KEY));
	}

	@Override
	public void apply() {
		_propertiesComponent.setValue(FILE_PATH_KEY,
									  _textFieldWithBrowseButton.getText());
		s_fileSupplierCache.put(_project.getLocationHash(),
								Suppliers.memoize(this::getFile));

	}

	@NotNull
	public File getCommunicationFile() {
		return s_fileSupplierCache.get(_project.getLocationHash()).get();
	}

	@NotNull
	private File getFile() {
		String savedFileName = _propertiesComponent.getValue(FILE_PATH_KEY);
		if (!Strings.isNullOrEmpty(savedFileName)) {
			return new File(savedFileName);
		}

		try {
			File tempFile = File.createTempFile(_project.getName(),
												".intellijCommandComm",
												new File(System.getProperty("java.io.tmpdir")));
			_propertiesComponent.setValue(FILE_PATH_KEY,
										  tempFile.getAbsolutePath());
			return tempFile;
		} catch (IOException e) {
			throw new RuntimeException();
		}
	}

}
