package code;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.compiler.CompilerManager;
import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class IntellijCommandListener implements ProjectComponent {

    private final CompilerManager _compilerManager;
    private final CommandListenerSettings _commandListenerSettings;
    private final Project _project;
    private boolean _shouldRun = true;

    public IntellijCommandListener(Project project) {
        _project = project;
        _commandListenerSettings = new CommandListenerSettings(project);
        _compilerManager = CompilerManager.getInstance(project);
    }

    @Override
    public void initComponent() {
        ApplicationManager.getApplication().executeOnPooledThread(
                () -> {
                    try {
                        while (_shouldRun) {
							BufferedCommandListener commandBuffer = CommandBuffer.createCommandListener(_commandListenerSettings
																												.getCommunicationFile());
                            commandBuffer.getNextCommand(this::scheduleCommand);
                            Thread.sleep(100);
                        }
                    } catch (IOException | InterruptedException e) {
                        e.printStackTrace();
                    }
                });
    }

	private void scheduleCommand(String command) {
        AutoResetEvent waitForUiThread = new AutoResetEvent();

		String upperCaseCommand = command.toUpperCase();
        if(upperCaseCommand.equals("REBUILD")) {
            ApplicationManager.getApplication().invokeLater(() -> _compilerManager.rebuild((aborted,
																								errors,
																								warnings,
																								compileContext) -> waitForUiThread.set()));
        } else if(upperCaseCommand.equals("BUILD")) {
            ApplicationManager.getApplication().invokeLater(() -> _compilerManager.make((aborted,
																							 errors,
																							 warnings,
																							 compileContext) -> waitForUiThread.set()));
        }

		try {
			waitForUiThread.waitOne();
		} catch (InterruptedException ignored) {

		}
	}

    @Override
    public void disposeComponent() {
        _shouldRun = false;
    }

    @NotNull
	@Override
    public String getComponentName() {
        return "Command Listener";
    }
}
