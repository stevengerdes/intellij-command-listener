package code;

import java.util.concurrent.locks.Lock;

public class AutoCloseableLock implements AutoCloseable{
	private Lock _lock;

	public AutoCloseableLock(Lock lock) {
		_lock = lock;
		_lock.lock();
	}

	@Override
	public void close() {
		_lock.unlock();
	}
}
