package code;

import java.util.concurrent.locks.*;

public class AutoResetEvent {
	private boolean _triggered;
	private final Lock _lock = new ReentrantLock();
	private Condition _condition = _lock.newCondition();

	public void set() {
		try (AutoCloseableLock l = new AutoCloseableLock(_lock)) {
			_triggered = true;
			_condition.signal();
		}
	}

	public void waitOne() throws InterruptedException {
		try (AutoCloseableLock ignored = new AutoCloseableLock(_lock)) {
			while (!_triggered) {
				_condition.await();
			}
			_triggered = false;
		}

	}
}
