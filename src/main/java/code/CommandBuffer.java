package code;

import com.intellij.util.Consumer;

import java.io.File;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;
import java.util.stream.IntStream;

abstract class CommandBuffer {

	private static final char CLEAR_BUFFER_CHAR = '\0';
	private static final char LAST_FULL_COMMAND_HAS_BEEN_RECIEVED = '\t';
	private static final int MAX_COMMAND_SIZE = 4096;
	protected CharBuffer _commandBuffer;

	protected CommandBuffer(File file) throws IOException {
		_commandBuffer = getCommandBuffer(file);
	}

	static BufferedCommandSender createCommandSender(File file) throws IOException {
		return new BufferedCommandSenderImpl(file);
	}

	static BufferedCommandListener createCommandListener(File file) throws IOException {
		return new BufferedCommandListenerImpl(file);
	}


	protected CharBuffer getCommandBuffer(File file) throws IOException {
		FileChannel channel = FileChannel.open(file.toPath(),
											   StandardOpenOption.READ,
											   StandardOpenOption.WRITE,
											   StandardOpenOption.CREATE);

		MappedByteBuffer b = channel.map(FileChannel.MapMode.READ_WRITE,
										 0,
										 MAX_COMMAND_SIZE);
		return b.asCharBuffer();
	}

	protected void clearBuffer(int length) {
		IntStream.range(0,
						length)
				 .forEach(i -> _commandBuffer.put(i,
												  CLEAR_BUFFER_CHAR));
	}

	private static class BufferedCommandSenderImpl extends CommandBuffer implements BufferedCommandSender {


		private BufferedCommandSenderImpl(File file) throws IOException {
			super(file);
		}

		private void waitForCommandAcknowledgment(int length) {
			Thread cleanUpBufferThread = new Thread(() -> clearBuffer(length));
			Runtime.getRuntime().addShutdownHook(cleanUpBufferThread);
			//noinspection StatementWithEmptyBody
			while (_commandBuffer.get(length - 1) != LAST_FULL_COMMAND_HAS_BEEN_RECIEVED) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					break;
				}
			}

			Runtime.getRuntime().removeShutdownHook(cleanUpBufferThread);

		}

		@Override
		public void sendCommand(String command) {
			_commandBuffer.put(command);
			try {
				waitForCommandAcknowledgment(command.length());
			} finally {
				clearBuffer(command.length());
			}

		}
	}

	private static class BufferedCommandListenerImpl extends CommandBuffer implements BufferedCommandListener {
		private BufferedCommandListenerImpl(File file) throws IOException {
			super(file);
		}


		private boolean noNewCommandExists() {
			char command = _commandBuffer.get(0);
			return command == CLEAR_BUFFER_CHAR || command == LAST_FULL_COMMAND_HAS_BEEN_RECIEVED;
		}

		private String getFullCommand() {
			char c;
			StringBuilder sb = new StringBuilder();
			while ((c = _commandBuffer.get()) != 0) {
				sb.append(c);
			}
			return sb.toString();
		}


		private void notifyCommandDone(String fullCommand) {
			IntStream.range(0,
							fullCommand.length())
					 .forEach(i -> _commandBuffer.put(i,
													  LAST_FULL_COMMAND_HAS_BEEN_RECIEVED));
		}

		@Override
		public void getNextCommand(Consumer<String> commandRunner) {
			if (noNewCommandExists()) {
				return;
			}

			String fullCommand = getFullCommand();
			clearBuffer(fullCommand.length());
			commandRunner.consume(fullCommand);
			notifyCommandDone(fullCommand);
		}
	}
}
