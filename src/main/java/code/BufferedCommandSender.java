package code;

public interface BufferedCommandSender {
	void sendCommand(String command);
}
