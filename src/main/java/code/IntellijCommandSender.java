package code;

import java.io.File;

public class IntellijCommandSender {

	public static void main(String[] args) throws Throwable {
		if( args.length != 2){
			System.out.println("Usage 'Java -jar nameOfThis.jar path/to/file/from/intellijCommandListenerPlugin/settings commandToSend");
			return;
		}
		BufferedCommandSender commandBuffer = CommandBuffer.createCommandSender(new File(args[0]));
		commandBuffer.sendCommand(args[1]);
	}

}