package code;

import com.intellij.util.Consumer;

public interface BufferedCommandListener {
	void getNextCommand(Consumer<String> scheduleCommand) throws InterruptedException;
}
